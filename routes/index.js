var http = require('request');
var cors = require('cors');
var uuid = require('uuid');
var url = require('url');
var _ = require('lodash');

module.exports = function (app, addon) {
  addon.hipchat = require('../lib/hipchat')(addon);

  // load botler routes
  require('./botler')(app, addon);

  // simple healthcheck
  app.get('/healthcheck', function (req, res) {
    res.send('OK');
  });

  // Root route. This route will serve the `addon.json` unless a homepage URL is
  // specified in `addon.json`.
  app.get('/',
    function (req, res) {
      // Use content-type negotiation to choose the best way to respond
      res.format({
        // If the request content-type is text-html, it will decide which to serve up
        'text/html': function () {
          var homepage = url.parse(addon.descriptor.links.homepage);
          if (homepage.hostname === req.hostname && homepage.path === req.path) {
            res.render('homepage', addon.descriptor);
          } else {
            res.redirect(addon.descriptor.links.homepage);
          }
        },
        // This logic is here to make sure that the `addon.json` is always
        // served up when requested by the host
        'application/json': function () {
          res.redirect('/atlassian-connect.json');
        }
      });
    });

  app.get('/glance',
      cors(),
      addon.authenticate(),
      function (req, res) {
        res.json({
          "label": {
            "type": "html",
            "value": "<b>BOTLER</b>"
          },
          "status": {
            "type": "lozenge",
            "value": {
              "label": "BETA",
              "type": "success"
            }
          }
        });
      });


  addon.on('installed', function (clientKey, clientInfo, req) {
    addon.hipchat.sendMessage(clientInfo, req.body.roomId, 'The ' + addon.descriptor.name + ' add-on has been installed in this room');
  });

  // Clean up clients when uninstalled
  addon.on('uninstalled', function (id) {
    addon.logger.info('Deleting client details of: ', id);
    addon.settings.del(id);
  });

};
