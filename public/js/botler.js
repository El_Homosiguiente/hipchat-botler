var PLS_NO = false;

$.fn.serializeObject = function() {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if (o[this.name] !== undefined) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  return o;
};

function showErrorMessage(title, message) {
  $('#aui-message-bar').empty();
  AJS.messages.error({
    title: title,
    body: message
  });
}

function createNewService(newServiceData, callback) {

  if (!(newServiceData && newServiceData.name && newServiceData.event && newServiceData.reply)) {
    showErrorMessage('Failed to create new service...', "Make sure you've entered all the required values!");
    callback(false)
    return;
  }

  HipChat.user.getCurrentUser(function(err, user) {
    if (err) {
      showErrorMessage('Something went wrong with HipChat', "Try again later! :(");
      callback(false)
    } else {
      HipChat.auth.withToken(function (err2, token) {
        if (!err2) {
          var owners = {};
          owners[user.id] = {name: user.name, mentionName: user.mention_name};
          newServiceData.owners = JSON.stringify(owners);
          $.ajax({
            type: 'POST',
            url: '/botler/room_service',
            headers: {'Authorization': 'JWT ' + token},
            dataType: 'json',
            data: newServiceData,
            complete: function (data) {
              var success = data.status == 200;
              if (!success) {
                showErrorMessage('Failed to create new service...', 'Sorry, something went terribly wrong.');
              }
              callback(success);
            }
          });
        }
      });
    }
  });  
}

function updateService(serviceData, callback) {

  if (!(serviceData && serviceData.name && serviceData.event && serviceData.reply)) {
    callback(false)
    return;
  }    

  HipChat.user.getCurrentUser(function(err, user) {
    if (err) {
      showErrorMessage('Something went wrong with HipChat', "Try again later! :(");
      callback(false)
    } else {
        serviceData.newOwner = JSON.stringify({id: user.id, name: user.name, mentionName: user.mention_name});        

        HipChat.auth.withToken(function (err2, token) {
          if (!err2) {
            $.ajax({
              type: 'PUT',
              url: '/botler/room_service',
              headers: {'Authorization': 'JWT ' + token},
              dataType: 'json',
              data: serviceData,
              complete: function (data) {
                var success = data.status == 200;
                if (!success) {
                  showErrorMessage('Failed to update your service...', 'Sorry, something went terribly wrong.');
                }
                callback(success);
              }
            });
          }
        });
    }
  });
}

function deleteService(serviceId, callback) {
  HipChat.auth.withToken(function (err, token) {
    if (!err) {
      $.ajax({
        type: 'DELETE',
        url: '/botler/room_service',
        headers: {'Authorization': 'JWT ' + token},
        dataType: 'json',
        data: {id: serviceId},
        success: function () {
          callback(true);
        },
        error: function () {
          callback(false);
        }
      });
    }
  });
}

function buttonClicked(event, closeDialog) {
  if (event.action === "botler.dialog.action") {
    console.log('creating...')
    if (!PLS_NO) {
      PLS_NO = true;
      var form = $('#new-botler-service');
      var newServiceData = form.serializeObject();

      delete newServiceData["id"];

      newServiceData.replyHtml = newServiceData.replyHtml === 'on' ? true:false;
      newServiceData.enable = newServiceData.enable === 'on' ? true : false;
      newServiceData.notify = newServiceData.notify === 'on' ? true : false;

      createNewService(newServiceData, function(success) {
        if (success) {
          console.log('created success');
          reloadSidebar();
        }
        closeDialog(success);
        PLS_NO = false;
        console.log('pls_no:', PLS_NO)
      });
    }
  } else if (event.action === "botler.dialog.save") {
    if (!PLS_NO) {
      PLS_NO = true;
      var form = $('#new-botler-service');
      var serviceData = form.serializeObject();

      serviceData.replyHtml = serviceData.replyHtml === 'on' ? true : false;
      serviceData.enable = serviceData.enable === 'on' ? true : false;
      serviceData.notify = serviceData.notify === 'on' ? true : false;

      updateService(serviceData, function (success) {

        if (success) {
          reloadSidebar();
        }
        closeDialog(success);
        PLS_NO = false;

      });
    }
  } else if (event.action === "botler.dialog.delete") {
    deleteService($('#new-botler-service #id').val(), function(success) {
      closeDialog(true);
      reloadSidebar();
    });
  } else {
    closeDialog(true);
  }
}

function loadService(id) {
  HipChat.auth.withToken(function (err, token) {
    if (!err) {
      $.ajax(
        {
          type: 'GET',
          url: '/botler/room_service',
          headers: {'Authorization': 'JWT ' + token},
          dataType: 'json',
          data: {id: id},
          success: function (data) {            
            $('#new-botler-service #id').val(data.id);
            $('#new-botler-service #name').val(data.name);
            AJS.$("#event").auiSelect2('val', data.event);
            if (data.event === 'room_message') {
              $('#event-params').fadeIn();
            } else {
              $('#event-params').fadeOut();
            }
            loadAttributesFor(data.event);
            $('#new-botler-service #pattern').val(data.pattern);
            $('#new-botler-service #reply').val(data.reply);
            $('#new-botler-service #replyHtml').prop('checked', data.replyHtml);
            $('#new-botler-service #enable').prop('checked', data.enable);
            $('#new-botler-service #notify').prop('checked', data.notify);
          },
          error: function () {
            console.log('failed');
          }
        });
      }
    });
  }

  function receiveParameters(parameters) {
    // This function will receive the parameters that were used in dialog.open(...)
    if (parameters && parameters.serviceId) {
      loadService(parameters.serviceId);
    } else {
      loadAttributesFor('room_message');
    }
  }

  function reloadSidebar() {
    HipChat.sidebar.closeView();
    HipChat.sidebar.openView({
      key: 'botler.sidebar'
    });
  }

  function appendMacroToReply(attribute) {
    $('#reply').val($('#reply').val() + '{{' + attribute + '}}');
  }

  function loadAttributesFor(event) {
    $.ajax({
      type: 'GET',
      url: '/botler/attributes?event=' + event,
      dataType: 'json',
      complete: function (data) {
        $('#botler-attributes').hide();
        AJS.$(".botler-attribute").tooltip('destroy');
        $('#botler-attributes').html(data.responseText);
        AJS.$(".botler-attribute").tooltip();
        $('#botler-attributes').fadeIn();
      }
    });
  }

  $(document).ready(function () {
    AP.register({
      "dialog-button-click": buttonClicked,
      "receive-parameters": receiveParameters
    });

    // setup the event select2 dropdown
    AJS.$("#event").auiSelect2();
    $("#event").on("change", function (e) {
      var event = $("#event option:selected").val();
      if (event === 'room_message') {
        $('#event-params').fadeIn();
      } else {
        $('#event-params').fadeOut();
      }

      loadAttributesFor(event);
    });
  });
