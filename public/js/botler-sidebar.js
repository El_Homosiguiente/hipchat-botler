function openNewBlockDialog() {
  AP.require("dialog", function (dialog) {
    dialog.open({
      key: "botler.dialog"
    })
  });
}

function openEditServiceDialog(id) {
  AP.require("dialog", function (dialog) {
    dialog.open({
      key: "botler.dialog",
      options: {
        options: {
          primaryAction: {
            name: "Save",
            key: "botler.dialog.save"
          },
          secondaryActions: [
            {
              name: "Delete",
              key: "botler.dialog.delete"
            },
            {
              name: "Close",
              key: "botler.dialog.close"
            }
          ]
        }
      },
      parameters: {
        serviceId: id
      }
    })
  });
}
